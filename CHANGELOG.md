# Changelog

All notable changes to this project will be documented in this file.

The format is based on
[Keep a Changelog](https://keepachangelog.com/en/1.0.0/),

The semantic of the versioning scheme is similar to
[Semantic Versioning 2.0.0](https://semver.org/spec/v2.0.0.html)

## [1.0.0] 2019-10-28

### Added

- this CHANGELOG.md to provide an easy to read overview of the
  current project status
- README.md to describe the behavior of the dict_filter functions
- dict_filter.cutKeys to cut off specific keys
- dict_filter.passKeys to only keep specific keys
- LICENSE file
- version.py so it is easy to get the `__version__` of this project
- .gitignore to ignore all files that are environment specific, caches and so on
