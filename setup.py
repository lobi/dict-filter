import setuptools
import dict_filter

setuptools.setup(
    name='dict_filter',
    version=dict_filter.__version__,
    author_email='s.lobinger@t-online.de',
    author='Sebastian Lobinger',
    description='dict_filter - a package to provide easy utp read filters for dict.',
    packages=(
        'dict_filter',
    ),
    classifiers=[
        "Programming Language :: Python :: 3",
        "License :: OSI Approved :: MIT License",
        "Operating System :: OS Independent",
    ],
)