import unittest

from .. import dict_filter


class TestDictFilter(unittest.TestCase):

    def setUp(self):
        self.raw_dict = {
            'A': 'a',
            'B': 'b',
            'C': 'c',
            'D': 'd',
            'E': 'e'}

    def testPassKeys(self):
        """A copy of raw_dict but only with specified keys has to be created"""
        szenarios = [
            {'white_list': ['A', 'C', 'D'],
             'result': {'A': 'a', 'C': 'c', 'D': 'd'}, },
            {'white_list': [], 'result': {}}
        ]
        for szenario in szenarios:
            with self.subTest('szenario: ' + str(szenario)):
                passed_dict = dict_filter.passKeys(
                    raw_dict=self.raw_dict, keys=szenario['white_list'])
                self.assertEqual(szenario['result'], passed_dict)
                self.assertNotEqual(passed_dict, self.raw_dict)

    def testCutKeys(self):
        """A copy of raw_dict but without specified keys has to be created"""
        szenarios = [
            {'black_list': ['B', 'C', 'E'],
             'result': {'A': 'a', 'D': 'd'}, },
            {'black_list': [], 'result': self.raw_dict}
        ]
        for szenario in szenarios:
            with self.subTest('szenario: ' + str(szenario)):
                passed_dict = dict_filter.cutKeys(
                    raw_dict=self.raw_dict, keys=szenario['black_list'])
                self.assertEqual(szenario['result'], passed_dict)
